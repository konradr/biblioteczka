# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Clear db apart from "statuses"
#ActiveRecord::Base.establish_connection
#ActiveRecord::Base.connection.tables.each do |table|
#  next if table == 'schema_migrations' || table == 'statuses'
#  # SQLite
#  ActiveRecord::Base.connection.execute("DELETE FROM #{table}")
#end


if Status.all.empty?
  Status.create!(name: 'nothing', description: 'Brak')
  Status.create!(name: 'read', description: 'Przeczytana')
  Status.create!(name: 'interesting', description: 'Do przeczytania')
  Status.create!(name: 'not_interesting', description: 'Nie interesuje mnie')
end

b1 = Book.create!(title: 'Książka autora x', cover_url: 'book1.jpg')
b2 = Book.create!(title: 'Kolejna książka autora x', cover_url: 'book2.jpg')
b3 = Book.create!(title: 'Książka autora y', cover_url: 'book3.jpg')
b4 = Book.create!(title: 'Książka autorów x i y', cover_url: 'book4.jpg')
b5 = Book.create!(title: 'Książka autorów x, y, z', cover_url: 'book5.jpg')

a1 = Author.create!(name: 'Autor X')
a2 = Author.create!(name: 'Autor Y')
a3 = Author.create!(name: 'Autor Z')

b1.author_ids = a1.id
b2.author_ids = a1.id
b3.author_ids = a2.id
b4.author_ids = [a1.id, a2.id]
b5.author_ids = [a1.id, a2.id, a3.id]


50.times do |i|
  b = Book.create!(title: Faker::Company.name, cover_url: 'book' + (i%5+1).to_s + '.jpg')
  a = Author.create!(name: Faker::Name.name)
  b.author_ids = a.id
end


