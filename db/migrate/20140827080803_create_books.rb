class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :title
      t.string :cover_url
      t.integer :status_id

      t.timestamps
    end
  end
end
