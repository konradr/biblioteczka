class Book < ActiveRecord::Base

  has_and_belongs_to_many :authors
  belongs_to :status
  
  before_save :default_values
  
  validates_presence_of :title, message: 'Nie podano tytułu'
  validates_uniqueness_of :title, message: 'Wybrana pozycja już istnieje'
  validates :cover_url, allow_blank: true, format: {
    with: %r{\.(gif|jpg|png)\Z}i,
    message: 'Zdjęcie okładki musi byc typu gif, jpg lub png.'
  }
  validates_associated :authors, :status
  
  def default_values
    self.status_id ||= 1
  end
  
  def self.search(search)
    if search
      where('title LIKE ?', "%#{search}%")
    end
  end
end
