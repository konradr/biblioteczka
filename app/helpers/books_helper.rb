module BooksHelper
  def sortable(column, title)  
     if (column == sort_column && sort_direction == "asc")
       direction = "desc"
     else
      direction = "asc"
     end
    link_to title, params.merge(sort: column, direction: direction, page: nil)
  end  
end
